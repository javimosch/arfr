#ARFR

ARFR is a boilerplate project for static site generated rojects.
ARFR is also the playground for MisitioBA.

 - Develop using npm run dev or dev:client dev:server (better error handling during development)
 - Generate a client side release using npm run build
 - Host in a NodeJS supported enviroment using npm run start

## HowTo?

This repository lacks with a lot of info about. For now, the only way is to follow by analogy.
For static sites check src/client/ba
For web application (angular) check meetful or storeco.
Let me know about any question by opening an issue or writing to arancibiajav@gmail.com

## Features

 - Static site generator
 - Ftp deploy
 - Live sync for javascript, html and css (dynamic injection)
 - Browser dev console css save (dynamic injection)
 - Heroku compatible. Just push the app and is working.
 - i18n
 - Mongoose wrapper (client side controllers!)
 - SCSS (SaaS compiler)
 - Html uglify
 - css uglify
 - js uglify
 
## Work in progress

 - Development GUI
 - Token security (API / Angular components)
 - Enviromental variables 'Hot reload'
 - CSS 'Hot reload'
 - JS  'Hot reload' (angular / redom)
 - HTML 'Hot reload' (static files injection via firebase)

## Enviromental vars

PORT=8080
PROD=1 //Production will trigger minification, uglification
APP=ba //Project to build /src/client/[project-name] /src/server/implementations/[project-name]
dbURI= //Mongoose uri
MULTILANG=0 //Multilang support
APP_ROUTING=0 //Routes will redirect to app/index.html (For angular or react applications)
ROLLUP=0 //Use of rollup for bundling
BUILD_SCRIPTS=0 //Disable scripts bundling (for static sites)

## Requirements

 - Node 9.5.0

 
