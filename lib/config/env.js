require('dotenv').config({
    silent: true,
    path: process.cwd() + '/.env'
});

if(!process.env.APP_NAME && !process.env.APP) throw new Error('env APP_NAME or APP required');

const PROD = process.env.PROD && process.env.PROD.toString() == '1' || process.env.NODE_ENV==='production' || false;
var path = require('path');
var resolver = require(path.join(process.cwd(), 'lib/resolver'));
var self = {
    LOGGER_WRITE_TO_FILE: process.env.LOGGER_WRITE_TO_FILE === '1' || false,
    LOGGER_FILE_PATH: process.env.LOGGER_FILE_PATH || process.cwd() + '/logs/logger.log',
    WEBPACK_CONFIG: process.env.WEBPACK_CONFIG || path.join(process.cwd(), 'lib/webpack.config.js'),
    WEBPACK: process.env.WEBPACK === '1' || false,
    OUTPUT_FOLDER: process.env.OUTPUT_FOLDER || "dist",
    JS_BUNDLE_PATH: "js/bundle.js",
    PROD: PROD,
    HOT_MAQUETTE: process.env.HOT_MAQUETTE && process.env.HOT_MAQUETTE.toString() == '1' || false,
    MULTILANG: process.env.MULTILANG && process.env.MULTILANG.toString() == '1' || false,
    ROLLUP: process.env.ROLLUP && process.env.ROLLUP.toString() == '1' || false,
    APP_NAME: process.env.APP_NAME || process.env.APP,
    APP_ROUTING: process.env.APP_ROUTING && process.env.APP_ROUTING.toString() == '1' || false,
    PORT: process.env.PORT || 3000,
    BUILD_SCRIPTS: process.env.BUILD_SCRIPTS && process.env.BUILD_SCRIPTS.toString() == '1' || false,
    BUILD: process.env.BUILD,
    DISABLE_WATCH: 0,
    DISABLE_FIREBASE: 0,
    PUBLIC_PATH: '', //Dynamic set
    NODE_ENV: process.env.NODE_ENV || 'development'
};
dynamicSet();
Object.assign(self, require(path.join(process.cwd(), 'src/server/envs')));
self.set = (obj) => {
    Object.assign(self, obj);
    dynamicSet();
    console.log('ENV', JSON.stringify(self));
};

function dynamicSet() {
    self.DEST = self.OUTPUT_FOLDER = self.PUBLIC_PATH = self.PROD ? 'dist-production' : 'dist';
}



var reload = require('require-reload')(require);
var CONSTANTS = require(path.join(process.cwd(), 'lib/constants'));
let fsFacade = require(path.join(process.cwd(), 'lib/facades/' + 'fs' + '-facade'));
let pathTo = (relativePath, joinPath) => path.join(process.cwd(), relativePath, joinPath || '');
var appContextPath = pathTo(CONSTANTS.SRC_CLIENT_PATH, path.join(self.APP_NAME, "context.js"));
var exists = fsFacade.existsSync(appContextPath);
if (exists) {
    var ctx = reload(appContextPath);
    if (ctx && ctx.env) {
        for (var x in ctx.env) {
            self[x] = ctx.env[x];
        }
    }
}


console.log('NODE_ENV', self.NODE_ENV);


module.exports = self;
