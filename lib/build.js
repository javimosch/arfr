"use strict";
var resolver = require('./resolver');
var path = require("path");
var logger = resolver.logger().get('BUILD', '');
resolver.co(function*() {

    logger.debugTerminal('START');

    resolver.env().set({
        PROD: true,
        DISABLE_WATCH: 1,
        DISABLE_FIREBASE: 1,
    });

    yield resolver.generator().configure();

    logger.debugTerminal('Move assets to production folder');
    var srcPath = path.join(process.cwd(), resolver.CONSTANT().SRC_CLIENT_PATH, resolver.env().APP_NAME, 'res');
    yield resolver.getFacade('fs').copy(srcPath, resolver.handlebarsContext()().outputBaseDir());

    logger.debugTerminal('Copy vendor assets');
    var srcPath = path.join(process.cwd(), 'vendor');
    yield resolver.getFacade('fs').copy(srcPath, path.join(resolver.handlebarsContext()().outputBaseDir(), 'vendor'));

    logger.debugTerminal('Copy common assets');
    var srcPath = path.join(process.cwd(), resolver.CONSTANT().SRC_CLIENT_PATH, 'common');
    yield resolver.getFacade('fs').copy(srcPath, path.join(resolver.handlebarsContext()().outputBaseDir(), 'common'));

    logger.debugTerminal('SUCCESS');

}).catch(resolver.errorHandler(logger.error));
