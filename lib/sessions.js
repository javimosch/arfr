var path = require('path');
var Promise = require('promise');
var resolver = require(path.join(process.cwd(), 'lib/resolver'));
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);

module.exports = {
    configure: (app) => {
        return resolver.coWrap(function*() {

            var store = new MongoDBStore({
                uri: resolver.env().DB_URI,
                collection: 'express_sessions'
            });

            // Catch errors 
            store.on('error', function(error) {
                console.log('SESSIONS ERROR', error);
            });

            app.use(require('express-session')({
                secret: 'This is a secret',
                cookie: {
                    maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week 
                },
                store: store,
                // Boilerplate options, see: 
                // * https://www.npmjs.com/package/express-session#resave 
                // * https://www.npmjs.com/package/express-session#saveuninitialized 
                resave: true,
                saveUninitialized: true
            }));

            return resolver.Promise.resolve(true);
        })();
    }
};
