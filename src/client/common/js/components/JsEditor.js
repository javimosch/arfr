import Preact, { Component, h } from 'preact'
/** @jsx h */

import createObjectURL from 'create-object-url';
window.URL.createObjectURL = createObjectURL;

import ace from 'brace';
import 'brace/mode/javascript';
import 'brace/theme/monokai';



export default class JsEditor extends Component {
    componentDidMount() {
        var editor = window.ace.edit("editor");
        editor.setTheme("ace/theme/monokai");
        editor.getSession().setMode("ace/mode/javascript");
    }
    render() {
        return (<div id="editor">
        console.log('Hola Mundo');
        </div>);
    }
}
